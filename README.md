# Getting Started


```bash
# Execute comand download dependencies and build jar
mvn clean install

# Launch app
java -jar target/user-register-0.0.1-SNAPSHOT.jar

# Check Url Swagger
http://localhost:9091/users-app/swagger-ui.html


# Request example create user
curl --location 'http://localhost:9091/users-app/api/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    
    "name": "Juan",    
    "email": "jrossa@gmail.com.co",
    "password": "Abcd123!",    
    "phones": [
        {
        "number": "1234567",
        "citycode": "1",
        "contrycode": "57"
        }
    ]
}'


