package com.register.user.userregister.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.register.user.userregister.entity.ConfigApp;
import com.register.user.userregister.repository.IConfigAppRepository;

/**
 * Service to acces Config param app
 * @author Usuario
 *
 */
@Service
public class ConfigAppServiceImpl implements IConfigAppService {
	
	@Autowired
	private IConfigAppRepository configAppRepository;

	/**
	 * Get param  app cacheable
	 */
	@Cacheable("configAppCache")
	@Override
	public Optional<ConfigApp> findByNameParam(String name) {		
		return configAppRepository.findByNameParam(name);
	}


}
