package com.register.user.userregister.services;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.crypto.KeyGenerator;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.register.user.userregister.dto.UserRequest;
import com.register.user.userregister.entity.ConfigApp;
import com.register.user.userregister.entity.Phone;
import com.register.user.userregister.entity.User;
import com.register.user.userregister.exceptions.ExcepcionApi;
import com.register.user.userregister.repository.IUserRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.InvalidKeyException;

@Service
public class UserServiceImpl implements IUserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	private IConfigAppService configAppService;

	@Value("${jwt.expiration}")
	private int jwtExpiration;
	
	private static final String ID_VALIDATE_CONFIG_PASSWORD="Regex_validate_pasword";

	@Value("${regexvalidate.email}")
	private String regexEmail;
	

	@Autowired
	ModelMapper modelMapper;

	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	@Transactional
	public User save(UserRequest user) throws ExcepcionApi {		
		validateEmail(user.getEmail());
		validateExistEmail(user.getEmail());
		validatePassword(user.getPassword());
		User us = new User();
		us = modelMapper.map(user, User.class);
		if (us.getPhones() != null) {
			for (Phone phone : us.getPhones()) {
				phone.setUser(us);
			}
		}
		us.setCreated(new Date());
		us.setLastLogin(us.getCreated());
		us.setIsactive(true);
		try {
			us.setToken(generateToken(user.getEmail()));
		} catch (InvalidKeyException | NoSuchAlgorithmException e) {
			log.error("Error generate token {}", e.getMessage());
		}
		return userRepository.save(us);
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(UUID idUser) {
		return userRepository.findById(idUser).orElse(null);
	}

	@Override
	@Transactional
	public void delete(UUID idUser) {
		log.debug("User Deleted {} ", idUser);
		userRepository.deleteById(idUser);

	}

	/**
	 * Generate Token
	 * @param username
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 */
	private String generateToken(String username) throws InvalidKeyException, NoSuchAlgorithmException {
		Date expirationDate = new Date(System.currentTimeMillis() + jwtExpiration * 1000);
		String token = Jwts.builder().setSubject(username).setIssuedAt(new Date()).setExpiration(expirationDate)
				.signWith(generateSecretKey(), SignatureAlgorithm.HS256).compact();
		return token;
	}

	/**
	 * Generatic automatic secretKey
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private static Key generateSecretKey() throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("HmacSHA256");
		SecureRandom secureRandom = new SecureRandom();
		keyGenerator.init(secureRandom);
		return keyGenerator.generateKey();
	}

	/**
	 * Validate Email
	 * @param email
	 * @throws ExcepcionApi
	 */
	private void validateEmail(String email) throws ExcepcionApi {
		if (!email.matches(regexEmail)) {
			throw new ExcepcionApi("Email does not contain a valid format");
		}
	}

	/**
	 * Validate exist email in data base
	 * @param email
	 * @throws ExcepcionApi
	 */
	private void validateExistEmail(String email) throws ExcepcionApi {
		if (userRepository.existsByEmail(email)) {
			throw new ExcepcionApi("The email now registered");
		}
	}

	/**
	 * Validate Format password
	 * @param password
	 * @throws ExcepcionApi
	 */
	private void validatePassword(String password) throws ExcepcionApi {		
		ConfigApp con=configAppService.findByNameParam(ID_VALIDATE_CONFIG_PASSWORD).orElse(null);
		if (con!=null && !password.matches(con.getValueParam())) {
			throw new ExcepcionApi("Password does not contain a valid format");
		}
	}
}
