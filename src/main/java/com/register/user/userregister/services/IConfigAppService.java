package com.register.user.userregister.services;

import java.util.Optional;

import com.register.user.userregister.entity.ConfigApp;

public interface IConfigAppService {
	
	/**
	 * Get param to database
	 * @param name
	 * @return
	 */
	Optional<ConfigApp> findByNameParam(String name);

}
