package com.register.user.userregister.services;

import java.util.List;
import java.util.UUID;

import com.register.user.userregister.dto.UserRequest;
import com.register.user.userregister.entity.User;
import com.register.user.userregister.exceptions.ExcepcionApi;

public interface IUserService {

	/**
	 * Service to find all user in database
	 * 
	 * @return
	 */
	public List<User> findAll();

	/**
	 * Service to persist user in database
	 * 
	 * @param user
	 * @return
	 */
	public User save(UserRequest user) throws ExcepcionApi;

	/**
	 * Service to find User by Id
	 * 
	 * @param idUser
	 * @return
	 */
	public User findById(UUID idUser);

	/**
	 * Service to delete User by Id
	 * 
	 * @param idUser
	 */
	public void delete(UUID idUser);

}
