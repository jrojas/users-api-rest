package com.register.user.userregister.exceptions;

/**
 * Excepcion aplication
 * @author Usuario
 *
 */
public class ExcepcionApi extends Exception {

	private static final long serialVersionUID = 1L;

	public ExcepcionApi(String message) {
		super(message);
	}

}
