package com.register.user.userregister.config.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.register.user.userregister.dto.MessageResponse;
import com.register.user.userregister.dto.UserRequest;
import com.register.user.userregister.dto.UserResponse;
import com.register.user.userregister.entity.User;
import com.register.user.userregister.exceptions.ExcepcionApi;
import com.register.user.userregister.services.IUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
/**
 * Controller User
 * @author Usuario
 *
 */
@RestController
@RequestMapping(value = "api/users")
public class UserController {
	@Autowired
	IUserService userService;

	@Autowired
	ModelMapper modelMapper;

	/**
	 * Service to create User
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "")
	@ApiOperation(value = "Create User", notes = "Create user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succes", response = UserResponse.class),
			@ApiResponse(code = 400, message = "User Bad Reques", response = MessageResponse.class),
			@ApiResponse(code = 500, message = "Internal server error processing the operation.", response = MessageResponse.class) })
	public ResponseEntity<Object> createUser(@Valid @RequestBody UserRequest user) {
		try {
			UserResponse us = modelMapper.map(userService.save(user), UserResponse.class);
			return new ResponseEntity<>(us, HttpStatus.CREATED);
		} catch (ExcepcionApi e) {
			return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Service get all users whith the tasks
	 * 
	 * @return
	 */
	@GetMapping("")
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ApiOperation(value = "Get all users", notes = "Get all users whith the tasks.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succes"),
			@ApiResponse(code = 400, message = "User Bad Reques", response = MessageResponse.class),
			@ApiResponse(code = 500, message = "Internal server error processing the operation.")

	})
	public ResponseEntity<List<User>> getUsers() {
		return ResponseEntity.ok(userService.findAll());
	}

	/**
	 * Service Get user by Id whith the tasks.
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{idUser}")
	@ApiOperation(value = "Get User by Id", notes = "Get user by Id whith the tasks.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succes"),
			@ApiResponse(code = 400, message = "User Bad Reques"),
			@ApiResponse(code = 500, message = "Internal server error processing the operation."),
			@ApiResponse(code = 404, message = "User not Found") })
	public ResponseEntity<Object> getUser(@PathVariable UUID idUser) {
		User user = userService.findById(idUser);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(userService.findById(idUser));
	}

	/**
	 * Service to Delete user
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/{idUser}")
	@ApiOperation(value = "Delete User", notes = "Delete user.")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Succes"),
			@ApiResponse(code = 400, message = "User Bad Reques"),
			@ApiResponse(code = 500, message = "Internal server error processing the operation."),
			@ApiResponse(code = 404, message = "User not Found") })
	public ResponseEntity<Void> deleteUser(@PathVariable UUID idUser) {
		userService.delete(idUser);
		return ResponseEntity.noContent().build();
	}

}
