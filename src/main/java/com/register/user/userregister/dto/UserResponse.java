package com.register.user.userregister.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model Reponse to create User
 * @author Usuario
 *
 */
@ApiModel(description = "User response")
public class UserResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Id UUID  user", example = "b243c086-8dcc-4531-b99f-e2904fa0a0db")
	private UUID id;

	@ApiModelProperty(notes = "Date created", example = "2023-11-28")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date created;

	@ApiModelProperty(notes = "Date modified", example = "2023-11-28")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date modified;

	@ApiModelProperty(notes = "Date last Login", example = "2023-11-28")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date lastLogin;

	@ApiModelProperty(notes = "Token user", example = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqcm9hQGdtYWlsLmNvbSIsImlhdCI6MTcwMTE5NjQxNSwiZXhwIjoxNzAxMjAwMDE1fQ.OaummjY9jpsR2sgu4O3DEqVXekr-QSomG6-3hn4rTic")
	private String token;

	@ApiModelProperty(notes = "Active User", example = "true o false")
	private Boolean isactive;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

}
