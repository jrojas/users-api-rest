package com.register.user.userregister.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;

/**
 * Model request to create User
 * @author Usuario
 *
 */
public class UserRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Name User", example = "Jorge", required = true)
	private String name;

	@ApiModelProperty(notes = "Email User", example = "Jorge@gmail.com", required = true)
	private String email;

	/**
	 * The password must have the following characteristics: At least 8 characters
	 * long. At least one lowercase letter. At least one capital letter. At least
	 * one digit. At least one special character. Example Abcd123!
	 */
	@ApiModelProperty(notes = "The password must have the following characteristics:\r\n"
			+ "At least 8 characters long.\r\n" + "At least one lowercase letter.\r\n"
			+ "At least one capital letter.\r\n" + "At least one digit.\r\n"
			+ "At least one special character.", example = "Abcd123!", required = true)
	private String password;

	@ApiModelProperty(notes = "Email User", required = true)
	private List<PhoneRequest> phones;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<PhoneRequest> getPhones() {
		return phones;
	}

	public void setPhones(List<PhoneRequest> phones) {
		this.phones = phones;
	}

}
