package com.register.user.userregister.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * Model request to create phone to user
 * @author Usuario
 *
 */
public class PhoneRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "Number", example = "3124455554", required = true)
	private String number;

	@ApiModelProperty(notes = "City Code", example = "11002", required = true)
	private String citycode;

	@ApiModelProperty(notes = "Contry Code", example = "53", required = true)
	private String contrycode;

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getContrycode() {
		return contrycode;
	}

	public void setContrycode(String contrycode) {
		this.contrycode = contrycode;
	}

}
