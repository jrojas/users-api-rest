package com.register.user.userregister.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Model Response to create user
 * @author Usuario
 *
 */
@ApiModel(description = "Message Response")
public class MessageResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	public MessageResponse(String message) {
		super();
		this.message = message;
	}

	@ApiModelProperty(notes = "Message of requuest", example = "User not found", required = true)
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
