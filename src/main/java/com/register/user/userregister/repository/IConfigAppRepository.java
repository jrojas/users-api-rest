package com.register.user.userregister.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.register.user.userregister.entity.ConfigApp;

/**
 * Dao Repository User
 * 
 * @author Usuario
 *
 */
public interface IConfigAppRepository extends JpaRepository<ConfigApp, Long> {

	 Optional<ConfigApp> findByNameParam(String name);

}
