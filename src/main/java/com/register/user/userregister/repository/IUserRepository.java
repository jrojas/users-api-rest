package com.register.user.userregister.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.register.user.userregister.entity.User;
/**
 * Dao Repository User
 * @author Usuario
 *
 */
public interface IUserRepository extends JpaRepository<User, UUID> {

	boolean existsByEmail(String email);

}
