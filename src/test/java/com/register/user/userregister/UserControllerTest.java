package com.register.user.userregister;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.register.user.userregister.dto.PhoneRequest;
import com.register.user.userregister.dto.UserRequest;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

	private final static String URL = "/api/users";

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void createUser() throws Exception {
		PhoneRequest phone = new PhoneRequest();
		phone.setCitycode("27");
		phone.setContrycode("57");
		phone.setNumber("718515454");
		List<PhoneRequest> lsPhones = new ArrayList<PhoneRequest>();
		lsPhones.add(phone);
		UserRequest newUser = new UserRequest();
		newUser.setPhones(lsPhones);
		newUser.setEmail("data@gmail.com");
		newUser.setName("juan");
		newUser.setPassword("Abcd123!");

		mockMvc.perform(MockMvcRequestBuilders.post(URL).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(newUser)))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.token").isNotEmpty());

		mockMvc.perform(MockMvcRequestBuilders.get(URL)).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1)).andReturn();
		
		mockMvc.perform(MockMvcRequestBuilders.post(URL).contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(newUser)))
				.andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("The email now registered"));
		
	}

}
